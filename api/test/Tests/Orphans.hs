{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tests.Orphans where

import Prelude

import Data.Aeson                                    (Value (..))

import Test.QuickCheck
import Test.QuickCheck.Instances.Time                ()
import Test.QuickCheck.Instances.UnorderedContainers ()
import Test.QuickCheck.Instances.UUID                ()


import Test.Arbitrary                                ()
import Tracker.API                                   (TrackEvent (..), TrackSession (..))


-- | Value is not used, just taken and stored, so no need to test it
instance Arbitrary Value where arbitrary = pure $ Object mempty

instance Arbitrary TrackSession where
  arbitrary = TrackSession <$> arbitrary --  tsUuidUrl
                           <*> arbitrary --  tsOwner
                           <*> arbitrary --  tsInitiator
                           <*> arbitrary --  tsStartHost
                           <*> arbitrary --  tsStartTime
                           <*> arbitrary --  tsStartPayload
                           <*> arbitrary --  tsEndHost
                           <*> arbitrary --  tsEndTime
                           <*> arbitrary --  tsEndPayload
                           <*> arbitrary --  tsEvents

instance Arbitrary TrackEvent where
  arbitrary = TrackEvent <$> arbitrary -- teStartHost
                         <*> arbitrary -- teStartTime
                         <*> arbitrary -- teStartPayload
                         <*> arbitrary -- teEndHost
                         <*> arbitrary -- teEndTime
                         <*> arbitrary -- teEndPayload
