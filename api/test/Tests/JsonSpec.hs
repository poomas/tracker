module Tests.JsonSpec (spec) where

import RIO

import Data.Aeson            (FromJSON, ToJSON, encode, parseJSON, toJSON)
import Data.Aeson.Types      (parseEither)
import Data.String.Conv      (toS)
import Data.Typeable         (typeOf)
import Servant.Swagger       (validateEveryToJSON)
import Test.Hspec            (Spec, describe)
import Test.Hspec.QuickCheck (modifyMaxSize, modifyMaxSuccess, prop)
import Test.QuickCheck       hiding (Result, Success)

import Test.Arbitrary        ()
import Tests.Orphans         ()
import Tracker.API


spec :: Spec
spec = do
  describe "JSON to/from conversion"
    . modifyMaxSize    (const 20)
    . modifyMaxSuccess (const 30)
    $ do
      propJSON (Proxy :: Proxy TrackSession)
      propJSON (Proxy :: Proxy TrackEvent)

  describe "All routes: ToJSON matches ToSchema"
    . modifyMaxSize    (const 20)
    . modifyMaxSuccess (const 30)
    $ validateEveryToJSON trackerRoutesApi



propJSON :: forall a . (Arbitrary a, ToJSON a, FromJSON a, Show a, Eq a, Typeable a)
         => Proxy a -> Spec
propJSON _ = prop testName $ \(a :: a) ->
  let jsn = "with " <> toS (encode a)
   in counterexample jsn (parseEither parseJSON (toJSON a) === Right a)
  where
    testName = show ty <> " FromJSON/ToJSON checks"
    ty       = typeOf (undefined :: a)
