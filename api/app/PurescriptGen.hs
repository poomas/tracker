module PurescriptGen where

import Prelude

import Control.Applicative                 ((<|>))
import Control.Monad.Reader.Class          (MonadReader)
import Data.Proxy                          (Proxy (..))
import Data.Text                           (Text)
import Language.PureScript.Bridge          (PSType, SumType, buildBridge, haskType,
                                            mkSumType, order, psTypeParameters)
import Language.PureScript.Bridge.Builder  (BridgeData, BridgePart, (^==))
import Language.PureScript.Bridge.TypeInfo (Language (..), TypeInfo (..), typeName)
import Lens.Micro.Platform
import Servant.PureScript                  (HasBridge (..))

import Common.PurescriptGen                (baseUrlBridge, exportPureScriptTypes,
                                            poomasBridge, setModuleName)
import Tracker.API                         (TrackEvent (..), TrackSession (..))



baseTypesDir :: Text
baseTypesDir = "Host."

typeModuleDir :: Text
typeModuleDir = baseTypesDir <> "Type.Tracker."

exportPoomasToPS :: FilePath -> IO ()
exportPoomasToPS = exportPureScriptTypes trackerBridge trackerTypes

data TrackerBridge

instance HasBridge TrackerBridge where
  languageBridge _ = buildBridge trackerBridge

trackerTypes :: [SumType 'Haskell]
trackerTypes = [ eqOrdType (Proxy :: Proxy TrackSession)
               , eqOrdType (Proxy :: Proxy TrackEvent)
               ]

  where
    eqOrdType p = order p $ mkSumType p

trackerBridge :: BridgePart
trackerBridge = poomasBridge
  <|>  (typeName ^== "SomeCustomEvent" >> psTrackerType)
  <|> baseUrlBridge (baseTypesDir <> "Request")

  <|> setModuleName typeModuleDir "TrackSession" "Track"
  <|> setModuleName typeModuleDir "TrackEvent"   "Track"

  where
    psTrackerType :: MonadReader BridgeData m => m PSType
    psTrackerType = do
      inType <- view haskType
      params <- psTypeParameters
      pure TypeInfo { _typePackage    = ""
                    , _typeModule     = typeModuleDir <> "Tracker"
                    , _typeName       = inType ^. typeName
                    , _typeParameters = params
                    }
