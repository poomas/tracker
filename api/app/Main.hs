module Main where

import Prelude

import           System.Directory           (createDirectory, doesDirectoryExist)
import Control.Monad (unless)
import qualified Options.Applicative        as O (execParser, fullDesc, header, help,
                                                  helper, info, long, progDesc, short,
                                                  showDefault, strOption, value, (<**>))

import PurescriptGen (exportPoomasToPS)
import Tracker.API   (generateSwagger)


-- | Any cmd line arguments overrides config
newtype Args = Args
  { argOutputDir :: FilePath -- ^ directory where to output swagger and JS files
  } deriving (Show)

-- | Run application server
main :: IO ()
main = do
  Args{..} <- O.execParser pInfo
  let pursOutputDir = argOutputDir <> "/purescript"

  unlessM (doesDirectoryExist argOutputDir) $ do
    putStrLn $ "Creating directory `" <> argOutputDir <> "`"
    createDirectory argOutputDir

  putStrLn $ "Generated files will be stored in directory `" <> argOutputDir <> "`"
  generateSwagger $ argOutputDir <> "/swagger.json"

  exportPoomasToPS pursOutputDir
  -- exportPureScriptApi trackerRoutesApi pursOutputDir


  where
    pInfo = O.info (args O.<**> O.helper)
                 (O.fullDesc <> O.progDesc "API definition for tracker app"
                             <> O.header   "API definition")
    args = Args <$> O.strOption (O.long "outputDir" <> O.short 'o'
                              <> O.value "docs"     <> O.showDefault
                              <> O.help "Directory where to output swagger, PS and JS files")


unlessM :: Monad m => m Bool -> m () -> m ()
unlessM mbool action = mbool >>= flip unless action
