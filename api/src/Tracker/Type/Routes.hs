module Tracker.Type.Routes
  ( Owner
  , Initiator
  , Worker
  , Tag
  , EventResult (..)

  , UUIDPathPiece
  , OwnerPathPiece
  , InitiatorPathPiece
  , WorkerPathPiece
  , EventResultPathPiece
  )
where

import RIO

import Data.UUID                (UUID)
import Servant                  (Capture', Description, Required)


import Tracker.Type.EventResult (EventResult (..))

------------------------------------------------------------------------------------------
-- Aliases
------------------------------------------------------------------------------------------

type Owner     = Text
type Initiator = Text
type Worker    = Text
type Tag       = Text


------------------------------------------------------------------------------------------
-- Route parts
------------------------------------------------------------------------------------------

type UUIDPathPiece =
  Capture' '[Required, Description "Unique track session identifier"] "uuid" UUID
type OwnerPathPiece =
  Capture' '[Required, Description "Owner of track process"] "owner" Owner
type InitiatorPathPiece =
  Capture' '[Required, Description "Initiator of track process"] "initiator" Initiator
type WorkerPathPiece =
  Capture' '[Required, Description "Subject performed track event"] "worker" Worker
type EventResultPathPiece =
  Capture' '[Required, Description "Result of track process"] "initiator" EventResult
