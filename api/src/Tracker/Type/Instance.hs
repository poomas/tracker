{-# OPTIONS_GHC -fno-warn-orphans #-}
module Tracker.Type.Instance
  ( module X

  , HasTypedService (..)

  )
where


import Common.Client       (HasTypedService (..))
import Common.Type.Orphans as X
import Tracker.Client      (TrackerService, trackerServiceName)


instance HasTypedService TrackerService where getTypedServiceName = trackerServiceName
