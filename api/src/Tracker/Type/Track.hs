module Tracker.Type.Track where


import Tracker.Imports

import Data.Aeson           (FromJSON (..), ToJSON (..), genericParseJSON, genericToJSON)
import Data.Swagger         (ToSchema (..), description, fromAesonOptions,
                             genericDeclareNamedSchema, schema)
import Common.Type.Config   (jsonOpts)


-- | Holds all information about the tracked session with events accumulated
--   till the moment this record was created
data TrackSession = TrackSession
  { tsUuid         :: UUID          -- ^ unique identifier of tracked session
  , tsOwner        :: Text          -- ^ this tracked session is owned by this subject
  , tsInitiator    :: Text          -- ^ subject that initiated this tracked session
  , tsStartHost    :: String        -- ^ host that initiated the session
  , tsStartTime    :: UTCTime       -- ^ dateTime when session started
  , tsStartPayload :: Object        -- ^ unspecified JSON payload given on start
  , tsEndHost      :: Maybe String  -- ^ host that ended the session
  , tsEndTime      :: Maybe UTCTime -- ^ dateTime when session ended
  , tsEndPayload   :: Object        -- ^ unspecified JSON payload given on end
  , tsEvents       :: [TrackEvent]  -- ^ list of events in this session, excluding start
  } deriving (Show, Eq, Ord, Generic)
instance ToJSON   TrackSession where toJSON    = genericToJSON    $ jsonOpts 2
instance FromJSON TrackSession where parseJSON = genericParseJSON $ jsonOpts 2
instance ToSchema TrackSession where
  declareNamedSchema p = genericDeclareNamedSchema (fromAesonOptions $ jsonOpts 2) p
    & mapped.schema.description ?~ "Tracked session information and events"


-- | Holds information about an tracked event
data TrackEvent = TrackEvent
  { teStartHost    :: String        -- ^ host that initiated the event
  , teStartTime    :: UTCTime       -- ^ dateTime when event started
  , teStartPayload :: Object        -- ^ unspecified JSON payload given on start
  , teEndHost      :: Maybe String  -- ^ host that ended the event
  , teEndTime      :: Maybe UTCTime -- ^ dateTime when event ended
  , teEndPayload   :: Object        -- ^ unspecified JSON payload given on end
  } deriving (Show, Eq, Ord, Generic)

instance ToJSON   TrackEvent where toJSON    = genericToJSON    $ jsonOpts 2
instance FromJSON TrackEvent where parseJSON = genericParseJSON $ jsonOpts 2
instance ToSchema TrackEvent where
  declareNamedSchema p = genericDeclareNamedSchema (fromAesonOptions $ jsonOpts 2) p
    & mapped.schema.description ?~ "An event in tracked session"
