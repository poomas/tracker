module Tracker.Type.EventResult
  ( EventResult (..)

  , jsonOpts
  , schemaOpts

  )
where

import           RIO

import           Data.Aeson          as A
import qualified Data.Char           as C
import           Data.Swagger        as Swg (SwaggerType (..), ToParamSchema (..), enum_,
                                             type_)
import           Data.Swagger        as Swg (SchemaOptions (..), fromAesonOptions)
import           Lens.Micro.Platform as X hiding (at, (.=))
import           Servant.API         (FromHttpApiData (..), ToHttpApiData (..))





data EventResult =
    ResultSuccess
  | ResultPartial
  | ResultWarning
  | ResultError
  deriving (Show, Eq, Ord, Generic, Read)
instance ToJSON   EventResult where toJSON    = genericToJSON    $ jsonOpts 6
instance FromJSON EventResult where parseJSON = genericParseJSON $ jsonOpts 6
instance ToHttpApiData EventResult where
  toUrlPiece ResultSuccess = "success"
  toUrlPiece ResultPartial = "partial"
  toUrlPiece ResultWarning = "warning"
  toUrlPiece ResultError   = "error"
instance FromHttpApiData EventResult where
  parseQueryParam = \case "success" -> Right ResultSuccess
                          "partial" -> Right ResultPartial
                          "warning" -> Right ResultWarning
                          "error"   -> Right ResultError
                          _         -> Left "Invalid EventResult value"

instance ToParamSchema EventResult where
 toParamSchema _ = mempty
   & type_ .~ Just SwaggerString
   & enum_ ?~ ["success","partial","warning","error" ]








------------------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------------------

-- | Options for automatic FromJSON and ToJSON generation.
--  Just lower-cases the first letter of the constructor.
jsonOpts :: Int -> Options
jsonOpts n = defaultOptions
  { A.sumEncoding            = ObjectWithSingleField
  , A.unwrapUnaryRecords     = True
  , A.constructorTagModifier = fixJSONField n
  , A.fieldLabelModifier     = fixJSONField n
  }

-- | Options for defining Schema
schemaOpts :: Int -> SchemaOptions
schemaOpts n = (fromAesonOptions $ jsonOpts n)
  { Swg.constructorTagModifier = fixJSONField n
  , Swg.unwrapUnaryRecords     = True
  }


fixJSONField :: Int -> String -> String
fixJSONField n t = case drop n t of
                     (x:xs) -> C.toLower x : xs
                     _      -> []
