module Tracker.API
  ( module X

  , TrackerRoutes (..)
  , trackerRoutesApi
  , generateSwagger

  , Owner
  , Initiator
  , Worker
  , Tag
  , UUIDPathPiece
  , OwnerPathPiece
  , InitiatorPathPiece
  , WorkerPathPiece
  , EventResult (..)

  )
where

import Tracker.Imports      hiding (serveDirectoryWebApp)

import Data.Swagger         ()
import Servant.API.Generic  ((:-), ToServantApi, genericApi)


import Common.API.Admin     as X (CommonAdminRoutes (..))
import Common.API.Swagger   (commonGenSwagger)
import Tracker.API.List     as X (ListRoutes (..))
import Tracker.API.Log      as X (LogRoutes (..))
import Tracker.API.Track    as X (CRURoutes (..))
import Tracker.Type.Orphans as X ()
import Tracker.Type.Track   as X

import Tracker.API.Misc     as X (MiscRoute)

------------------------------------------------------------------------------------------
-- Routes/API
------------------------------------------------------------------------------------------

data TrackerRoutes r = TrackerRoutes
  { trackerCRUR   :: r :- "track" :> ToServantApi CRURoutes
  , trackerListR  :: r :- "list"  :> ToServantApi ListRoutes
  , trackerAdminR :: r :- "admin" :> ToServantApi CommonAdminRoutes
  , trackerLogR   :: r :- "log"   :> ToServantApi LogRoutes
  , trackerMiscR  :: r :- "misc"  :>              MiscRoute
  } deriving (Generic)


trackerRoutesApi :: Proxy (ToServantApi TrackerRoutes)
trackerRoutesApi = genericApi (Proxy :: Proxy TrackerRoutes)


-- | Output generated @swagger.json@ file.
generateSwagger :: FilePath -> IO ()
generateSwagger = commonGenSwagger
  "Tracker Swagger API"
  "API specification for poomas Tracker service"
  (genericApi (Proxy :: Proxy TrackerRoutes))
