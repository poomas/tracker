module Tracker.API.Log where

import Tracker.Imports

import Servant.API.Generic ((:-))

import Poomas.API          (LogFilterLoad (..))

------------------------------------------------------------------------------------------
-- Log control routes
------------------------------------------------------------------------------------------

-- curl -i -POST localhost:3333/debug/console --header "Content-Type: application/json" -d '{"severity":"Error"}'

-- | Set logging settings
type DynamicPermitPostRoute =
     Summary "Changes dynamically permit data for the Scribe in app"
  :> Capture' '[Description "Name of the scribe to modify or 'all' for all scribes"]
               "scribe"
                Text
  :> ReqBody '[JSON] LogFilterLoad
  :> Post '[JSON] Text

type DynamicPermitGetRoute =
     Summary "Return current log settings on Tracker"
  :> Get '[JSON] [(Text, LogFilterLoad)]


------------------------------------------------------------------------------------------
-- Routes
------------------------------------------------------------------------------------------
data LogRoutes r = LogRoutes
  { logSetR :: r :- DynamicPermitPostRoute
  , logGetR :: r :- DynamicPermitGetRoute
  } deriving (Generic)
