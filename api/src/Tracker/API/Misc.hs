module Tracker.API.Misc where

import Tracker.Imports

------------------------------------------------------------------------------------------
-- Listing routes
------------------------------------------------------------------------------------------

-- | Fetch a tracked session
type MiscRoute =
     Summary "Just a tester."
  :> Capture "port" Int
  :> Capture "msg" Text
  :> Get '[JSON] Text
