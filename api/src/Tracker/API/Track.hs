module Tracker.API.Track where

import Tracker.Imports

import Servant.API.Generic ((:-))


------------------------------------------------------------------------------------------
-- Start/end tracking session and add events to it
------------------------------------------------------------------------------------------

-- | Endpoint for initiating tracked session
type StartSessionPostRoute =
     Summary "Initiate new tracked session."
  :> RemoteHost
  :> OwnerPathPiece
  :> InitiatorPathPiece
  :> ReqBody '[JSON] Object
  :> Post '[JSON] UUID

-- | Endpoint for ending tracked session
type EndSessionPostRoute =
     Summary "End existing tracked session."
  :> RemoteHost
  :> UUIDPathPiece
  :> ReqBody '[JSON] Object
  :> Post '[JSON] UUID


-- | Endpoint for starting a new event
type StartEventPostRoute =
     Summary "Mark beginning of an event."
  :> RemoteHost
  :> UUIDPathPiece
  :> WorkerPathPiece
  :> ReqBody '[JSON] Object
  :> Post '[JSON] Int64

-- | Endpoint for ending the event
type EndEventPostRoute =
     Summary "Mark an end of the event."
  :> RemoteHost
  :> Capture' '[Required, Description "Event identifier"] "event" Int64
  :> EventResultPathPiece
  :> ReqBody '[JSON] Object
  :> Post '[JSON] Text




------------------------------------------------------------------------------------------
-- API
------------------------------------------------------------------------------------------

data CRURoutes r = CRURoutes
  { cruStartSessionR :: r :- "session" :> "start" :> StartSessionPostRoute
  , cruEndSessionR   :: r :- "session" :> "end"   :> EndSessionPostRoute
  , cruStartEventR   :: r :- "event"   :> "start" :> StartEventPostRoute
  , cruEndEventR     :: r :- "event"   :> "end"   :> EndEventPostRoute
  } deriving (Generic)
