module Tracker.API.List where

import Tracker.Imports

import Servant.API.Generic ((:-))


import Tracker.Type.Track (TrackSession)

------------------------------------------------------------------------------------------
-- Listing routes
------------------------------------------------------------------------------------------

-- | Fetch a tracked session
type ListSessionRoute =
     Summary "Fetch a tracked session."
  :> UUIDPathPiece
  :> Get '[JSON] TrackSession


-- | List tracked sessions by owner
type ListOwnerRoute =
     Summary "List all events of a track sessions for an owner."
  :> OwnerPathPiece
  :> Get '[JSON] [TrackSession]

-- | List tracked sessions by owner and initiator
type ListOwnerInitiatorRoute =
     Summary "List all events of a track sessions for an owner."
  :> OwnerPathPiece
  :> InitiatorPathPiece
  :> Get '[JSON] [TrackSession]



------------------------------------------------------------------------------------------
-- API
------------------------------------------------------------------------------------------
data ListRoutes r = ListRoutes
  { listSessionR        :: r :- "session"   :> ListSessionRoute
  , listOwnerR          :: r :- "owner"     :> ListOwnerRoute
  , listOwnerInitiatorR :: r :- "initiator" :> ListOwnerInitiatorRoute
  } deriving (Generic)
