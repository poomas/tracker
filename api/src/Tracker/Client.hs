module Tracker.Client where

import Tracker.Imports          hiding (ClientM, client)

import Servant.API.Generic
import Servant.Client.Generic
import Servant.Client.Streaming

import Common.API.Admin         (CommonAdminRoutes (..))
import Common.Client            (HasTypedService (..), runPoomasClient,
                                 runPoomasClientEither, withPoomasClient)
import Common.Type.App          (HasCommonApp (..))
import Poomas.API               (LogFilterLoad (..))
import Scribe.Orphans.Scribe    ()
import Tracker.API              (TrackerRoutes (..))
import Tracker.API.List         (ListRoutes (..))
import Tracker.API.Log          (LogRoutes (..))
import Tracker.API.Track        (CRURoutes (..))
import Tracker.Type.Orphans     ()
import Tracker.Type.Track       (TrackSession)
-- | Type specifying this service
trackerServiceName :: Text
trackerServiceName = "tracker"
data TrackerService


-- | Access and use Poomas functionality
type TrackerMonad m = ( MonadIO m
                      , MonadThrow m
                      , HasCommonApp m
                      , HasTypedService TrackerService
                      )

------------------------------------------------------------------------------------------
-- Running client functions
------------------------------------------------------------------------------------------

runTracker :: (TrackerMonad m, NFData a)  => ClientM a -> m a
runTracker = runPoomasClient @TrackerService

runTrackerEither :: (TrackerMonad m, NFData a) => ClientM a -> m (Either ClientError a)
runTrackerEither = runPoomasClientEither @TrackerService

withTracker :: TrackerMonad m => ClientM a -> (Either ClientError a -> IO b) -> m b
withTracker = withPoomasClient @TrackerService


------------------------------------------------------------------------------------------
-- Client functions
------------------------------------------------------------------------------------------
-- | Client functions derived via generics

trackerRoutes :: TrackerRoutes (AsClientT ClientM)
trackerRoutes = fromServant $ client (Proxy :: Proxy (ToServant TrackerRoutes AsApi))


-- | Admin route clients
adminRoutes :: CommonAdminRoutes (AsClientT ClientM)
adminRoutes = fromServant $ trackerAdminR trackerRoutes
setDebug :: Text -> Severity -> ClientM Text
setDebug = carDebugSetR adminRoutes
debugGet :: ClientM [(Text, Severity)]
debugGet = carDebugInfoR adminRoutes
shutdown :: ClientM Text
shutdown = carShutdownR adminRoutes


-- | Track routes
cruRoutes :: CRURoutes (AsClientT ClientM)
cruRoutes = fromServant $ trackerCRUR trackerRoutes
startSession :: Owner -> Initiator -> Object -> ClientM UUID
startSession = cruStartSessionR cruRoutes
endSession :: UUID -> Object -> ClientM UUID
endSession = cruEndSessionR cruRoutes
startEvent :: UUID -> Worker -> Object -> ClientM Int64
startEvent = cruStartEventR cruRoutes
endEvent :: Int64 -> EventResult -> Object -> ClientM Text
endEvent = cruEndEventR cruRoutes


-- | Listing routes
listRoutes :: ListRoutes (AsClientT ClientM)
listRoutes = fromServant $ trackerListR trackerRoutes
listTrack :: UUID -> ClientM TrackSession
listTrack = listSessionR listRoutes
listOwnerTracks :: Owner -> ClientM [TrackSession]
listOwnerTracks = listOwnerR listRoutes
listClients :: Owner -> Initiator -> ClientM [TrackSession]
listClients = listOwnerInitiatorR listRoutes

-- | Log routes
logRoutes :: LogRoutes (AsClientT ClientM)
logRoutes = fromServant $ trackerLogR trackerRoutes
logSet :: Text -> LogFilterLoad -> ClientM Text
logSet = logSetR logRoutes
logGet :: ClientM  [(Text, LogFilterLoad)]
logGet = logGetR logRoutes



------------------------------------------------------------------------------------------
-- Links
------------------------------------------------------------------------------------------
-- | Admin route clients
trackerLinks :: TrackerRoutes (AsLink Link)
trackerLinks = allFieldLinks

-- | Admin links
adminLinks           :: CommonAdminRoutes (AsLink Link)
adminLinks           = fromServant $ trackerAdminR trackerLinks
setDebugLevelLpost   :: Text -> Severity -> URI
setDebugLevelLpost a = linkURI . carDebugSetR adminLinks a
getDebugInfoLget     :: URI
getDebugInfoLget     = linkURI $ carDebugInfoR adminLinks
shutdownLget         :: URI
shutdownLget         = linkURI $ carShutdownR adminLinks

-- | Track Links
cruLinks :: CRURoutes (AsLink Link)
cruLinks = fromServant $ trackerCRUR trackerLinks
cruStartSessionLpost :: Owner -> Initiator -> Link
cruStartSessionLpost = cruStartSessionR cruLinks
cruEndSessionLpost :: UUID -> Link
cruEndSessionLpost  = cruEndSessionR cruLinks
cruStartEventLget :: UUID -> Worker -> Link
cruStartEventLget  = cruStartEventR cruLinks
cruEndEventLget :: Int64 -> EventResult -> Link
cruEndEventLget  = cruEndEventR cruLinks


-- | Listing Links
listLinks :: ListRoutes (AsLink Link)
listLinks = fromServant $ trackerListR trackerLinks
listTrackLget :: UUID -> Link
listTrackLget = listSessionR listLinks
listOwnerTracksLget :: Text -> Link
listOwnerTracksLget = listOwnerR listLinks
listClientsLget :: Text -> Text -> Link
listClientsLget = listOwnerInitiatorR listLinks

-- | Log Links
logLinks :: LogRoutes (AsLink Link)
logLinks = fromServant $ trackerLogR trackerLinks
logLset :: Text -> Link
logLset = logSetR logLinks
logLget :: Link
logLget = logGetR logLinks
