Tracker - poomas service
==============================================

Collects events happening in a tracked session and notifies the initiator on session completion.
