{-# OPTIONS_GHC -fno-warn-orphans #-}
module Lib.Type.Orphans ( module X ) where

import RIO

import Control.Error.Util     (hush)
import Data.Aeson             (Object)
import Data.String.Conv       (toS)
import Data.UUID              (UUID, fromASCIIBytes, toASCIIBytes)
import Database.Persist.Class ()
import Database.Persist.Sql   (PersistField (..), PersistFieldSql (..), PersistValue (..),
                               SqlType (..))
import Katip                  (LogItem, PayloadSelection (..), Verbosity (..),
                               payloadKeys)
import RIO.Orphans            as X
import Servant.API            (FromHttpApiData (..), ToHttpApiData (..))
import Web.PathPieces         (PathPiece (..))


import Common.Type.Orphans    as X
import Tracker.API            (EventResult)


------------------------------------------------------------------------------------------
-- UUID instances
------------------------------------------------------------------------------------------
instance PersistFieldSql UUID where sqlType _ = SqlOther "uuid"
instance PersistField    UUID where
  toPersistValue = PersistDbSpecific . toASCIIBytes
  fromPersistValue (PersistDbSpecific uuid) = case fromASCIIBytes uuid of
    Nothing -> Left $ "Bad UUID value: " <> tshow uuid
    Just u  -> Right u
  fromPersistValue v = Left $ "Not UUID - expected PersistDbSpecific - got: " <> tshow v

instance PathPiece       UUID where
  fromPathPiece = fromASCIIBytes . toS
  toPathPiece   = toS . toASCIIBytes


------------------------------------------------------------------------------------------
-- UUID instances
------------------------------------------------------------------------------------------
instance PersistFieldSql EventResult where sqlType _ = SqlString
instance PersistField    EventResult where
  toPersistValue                    = PersistText . toUrlPiece
  fromPersistValue (PersistText t ) = parseQueryParam t
  fromPersistValue x                = Left $ "Invalid EventResult: " <> tshow x
instance PathPiece       EventResult where
  fromPathPiece = hush . parseQueryParam
  toPathPiece   = toUrlPiece


------------------------------------------------------------------------------------------
-- Katip instances
------------------------------------------------------------------------------------------
instance LogItem Object where
    payloadKeys V0 _ = SomeKeys []
    payloadKeys V1 _ = SomeKeys []
    payloadKeys _ _  = AllKeys
