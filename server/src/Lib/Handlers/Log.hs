module Lib.Handlers.Log where

import Lib.Imports

import Common.Functions (setDynamicPermitData, dpdToLfl)
import Common.Type.App  (CommonApp (..), HasCommonApp (..), getDynPermMap, getPhcEnv)
import Poomas.API       (LogFilterLoad, MessageData (..), MessageLoad (..))
import Poomas.Server    (sendMessage)

-- | Handler for setting logging functionality
setDpdH :: (KatipContext m, HasCommonApp m) => Text -> LogFilterLoad -> m Text
setDpdH scribeName lfl = do
  logDebugKS "Tracker" "Set DPD"
  CommonApp{..} <- getCommonApp
  phcEnv        <- getPhcEnv
  let ml = MessageLoad
        { msgLFrom      = BaseUrl Http (toS capHost) (fromIntegral capPort) ""
        , msgLRcptOrder = 0
        , msgLData      = MessageLog lfl
        }

  _recipientsReceivedMassage <- liftIO $ sendMessage phcEnv [] ml

  bool "Error" "OK" <$> setDynamicPermitData scribeName lfl


getDpdH :: (KatipContext m, HasCommonApp m) => m [(Text, LogFilterLoad)]
getDpdH = do
  logDebugKS "Tracker" "Get DPD"
  map (second dpdToLfl) . mapToList <$> getDynPermMap
