module Lib.Handlers.Misc where

import Lib.Imports

import Common.Type.App

import Poomas.API
import Poomas.Server

getMisc :: (HasCommonApp m, MonadIO m) => Int -> Text -> m Text
getMisc port msg = do
  phcEnv <- getPhcEnv

  let ml = MessageLoad
        { msgLFrom      = mkUrl2 0
        , msgLRcptOrder = 0
        , msgLData      = MessageCustom msg
        }
-- sendMessage :: PhcEnv -> [Either BaseUrl Text] -> MessageLoad -> IO Int

  n <- liftIO $ sendMessage phcEnv [Left $ mkUrl2 port] ml
  pure $ "Message sent to " <> tshow n <> " recipients."

mkUrl2 :: Int -> BaseUrl
mkUrl2 p  = BaseUrl Http "localhost" p ""
