module Lib.Handlers.Track where

import Lib.Imports      hiding (Error)

import Data.IP          (fromSockAddr)
import Data.UUID.V4     (nextRandom)
import Database.Persist (PersistValue (..), entityKey, insert, keyFromValues, keyToValues,
                         update, (=.))
import Network.Socket   (SockAddr)

import Lib.ModelTH
import Tracker.API      (Initiator, Owner, Worker, EventResult)

------------------------------------------------------------------------------------------
-- Receive streamed file
------------------------------------------------------------------------------------------


-- bad  curl -i --request POST -H "Content-Disposition: attachment;filename=\"my_profile\";" --data-binary "@.profile" 'localhost:3030/du/CLI1/DOM1?tag=jura&tag=pero'

-- | Start a new tracked session
startSession :: (HasDbPool m, MonadIO m)
             => SockAddr -> Owner -> Initiator -> Object -> m UUID
startSession h o i pld = do
  uuid <- liftIO nextRandom
  now  <- liftIO getCurrentTime
  let track = SessionDb
        { sessionDbUuid         = uuid
        , sessionDbOwner        = o
        , sessionDbInitiator    = i
        , sessionDbStartHost    = getIP h
        , sessionDbStartTime    = now
        , sessionDbStartPayload = toJSON pld
        , sessionDbEndHost      = Nothing
        , sessionDbEndTime      = Nothing
        , sessionDbEndPayload   = object []
        }
  void . runDB $ insert track
  pure uuid

-- | End existing tracked session
endSession :: (HasDbPool m, MonadIO m) => SockAddr -> UUID -> Object -> m UUID
endSession h uuid pld = do
  now <- liftIO getCurrentTime
  runDB $ do
    key <- entityKey <$> getBy404 (UniqueUuid uuid)
    update key [ SessionDbEndHost    =. Just (getIP h)
               , SessionDbEndTime    =. Just now
               , SessionDbEndPayload =. toJSON pld
               ]
    pure uuid

-- | Add new event to existing tracked session
startEvent :: (HasDbPool m, MonadIO m) => SockAddr -> UUID -> Worker -> Object -> m Int64
startEvent h uuid worker pld = do
  key <- runDB $ entityKey <$> getBy404 (UniqueUuid uuid)
  now <- liftIO getCurrentTime
  let track = EventDb
                { eventDbUuid         = key
                , eventDbWorker       = worker
                , eventDbStartHost    = getIP h
                , eventDbStartTime    = now
                , eventDbStartPayload = toJSON pld
                , eventDbEndHost      = Nothing
                , eventDbEndTime      = Nothing
                , eventDbEndPayload   = object []
                , eventDbEndResult    = Nothing
                }
  runDB $ do
    pe <- insert track
    case keyToValues pe of
      [PersistInt64 k] -> pure k
      _                -> throw err500{errBody = "Impossible: Bad key conversion"}

-- | Add new event to existing tracked session
endEvent :: (HasDbPool m, MonadIO m)
         => SockAddr -> Int64 -> EventResult -> Object -> m Text
endEvent h evId evResult pld = do
  case keyFromValues [PersistInt64 evId] of
    Left  _     -> throw err404
    Right evKey -> do
      now <- liftIO getCurrentTime
      runDB $ do
        void $ get404 evKey
        update evKey [ EventDbEndHost    =. Just (getIP h)
                     , EventDbEndTime    =. Just now
                     , EventDbEndPayload =. toJSON pld
                     , EventDbEndResult  =. Just evResult
                     ]
      pure "OK"


getIP :: SockAddr -> String
getIP sa = maybe "IP N/A" (show . fst) $ fromSockAddr sa
