module Lib.Handlers.List where

import Lib.Imports

import Database.Persist (Entity (..), selectList, (==.))

import Lib.ModelTH
import Tracker.API      (Initiator, Owner, TrackEvent (..), TrackSession (..))


------------------------------------------------------------------------------------------
-- Listing routes
------------------------------------------------------------------------------------------

-- | Handler for listing all tracks belonging to a UUID
listSession :: (HasDbPool m, MonadIO m) => UUID -> m TrackSession
listSession uuid = runDB $ getSessionEvents =<< getBy404 (UniqueUuid uuid)


-- | Handler for listing all tracks belonging to an owner
listOwner :: (HasDbPool m, MonadIO m) => Owner -> m [TrackSession]
listOwner o = runDB $ mapM getSessionEvents =<< selectList [SessionDbOwner ==. o] []


-- | Handler for listing all tracks belonging to an owner and initiated by initiator
listOwnerInitiator :: (HasDbPool m, MonadIO m) => Owner -> Initiator -> m [TrackSession]
listOwnerInitiator o i = runDB $
  mapM getSessionEvents =<< selectList [SessionDbOwner ==. o, SessionDbInitiator ==. i] []


------------------------------------------------------------------------------------------
-- Helpers
------------------------------------------------------------------------------------------

toTrackSession :: SessionDb -> [EventDb] -> TrackSession
toTrackSession SessionDb{..} is = TrackSession
  { tsUuid         = sessionDbUuid
  , tsOwner        = sessionDbOwner
  , tsInitiator    = sessionDbInitiator
  , tsStartHost    = sessionDbStartHost
  , tsStartTime    = sessionDbStartTime
  , tsStartPayload = toObj sessionDbStartPayload
  , tsEndHost      = sessionDbEndHost
  , tsEndTime      = sessionDbEndTime
  , tsEndPayload   = toObj sessionDbEndPayload
  , tsEvents       = map toTrackEvent is
  }

  where
    toTrackEvent EventDb{..} = TrackEvent
      { teStartHost    = eventDbStartHost
      , teStartTime    = eventDbStartTime
      , teStartPayload = toObj eventDbStartPayload
      , teEndHost      = eventDbEndHost
      , teEndTime      = eventDbEndTime
      , teEndPayload   = toObj eventDbEndPayload
      }

    toObj (Object o) = o
    toObj _x         = mempty -- error ??

getSession :: UUID -> DbAction TrackSession
getSession uuid = do
  Entity k v <- getBy404 (UniqueUuid uuid)
  toTrackSession v .  map entityVal <$> selectList [EventDbUuid ==. k] []


getSessionEvents :: Entity SessionDb -> DbAction TrackSession
getSessionEvents (Entity uuid sess) = toTrackSession sess
                                    .  map entityVal
                                   <$> selectList [EventDbUuid ==. uuid] []
