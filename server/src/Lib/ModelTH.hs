{-# LANGUAGE EmptyDataDecls, FlexibleContexts, GADTs, GeneralizedNewtypeDeriving,
             OverloadedStrings, QuasiQuotes, TemplateHaskell, TypeFamilies,
             UndecidableInstances #-}

module Lib.ModelTH where

import Lib.Imports

import Database.Persist.Postgresql.JSON ()
import Database.Persist.TH              (mkMigrate, mkPersist, persistLowerCase, share,
                                         sqlSettings)

import Lib.Type.Orphans                 ()
import Tracker.API                      (EventResult, Initiator, Owner, Worker)


share [mkPersist sqlSettings, mkMigrate "migrateAll"] [persistLowerCase|
SessionDb
  uuid         UUID
  owner        Owner
  initiator    Initiator
  startHost    String
  startTime    UTCTime
  startPayload Value
  endHost      String Maybe
  endTime      UTCTime Maybe
  endPayload   Value
  Primary      uuid
  UniqueUuid   uuid
  deriving Show Read Generic

EventDb
  uuid         SessionDbId
  worker       Worker
  startHost    String
  startTime    UTCTime
  startPayload Value
  endHost      String Maybe
  endTime      UTCTime Maybe
  endPayload   Value
  endResult    EventResult Maybe
  deriving Show Read Generic
|]
