module App.Application
  ( App (..)
  , AppConfig (..)

  , prepConfig
  , prepApp
  )
where

import App.Imports

import Control.Monad.Except        (ExceptT (..))
import Control.Monad.Logger        (runNoLoggingT, runStdoutLoggingT)
import Database.Persist.Postgresql (runMigration)
import Network.Wai                 (Application, Middleware)
import Servant.API.Generic         (ToServant, ToServantApi)
import Servant.Ekg                 (HasEndpoint)
import Servant.Server.Generic      (AsServerT, genericServerT)

import App.Routes                  (cruApi, listApi, logApi)
import Common.API.Routes           (commonAdminApi)
import Common.DbStartupLog         (dbStartupLog)
import Common.Run                  (AppPrep (..), prepCommonApp)
import Common.StartupLog           (startupLog)
import Common.Type.App             (mkCommonApp)
import Lib.ModelTH                 (migrateAll)
import Poomas.Server               (PhcEnv)
import Tracker.API                 (TrackerRoutes)

import Lib.Handlers.Misc

-- | Record of 'Handler's for the whole API
trackerApi :: ToServant TrackerRoutes (AsServerT AppM)
trackerApi = genericServerT TrackerRoutes
  { trackerCRUR   = cruApi
  , trackerListR  = listApi
  , trackerAdminR = commonAdminApi
  , trackerLogR   = logApi
  , trackerMiscR  = getMisc
  }


prepConfig :: FilePath -> IO AppConfig
prepConfig cfgFile = do
  isDev <- ("production" `notElem`) <$> getArgs
  parseCommonConfig isDev [cfgFile] []

-- | Start the application server. It is expected to run the server as a forked
--   process, and `shutdownSwitch` enables "almost" clean shutting it down. It is
--   an empty MVar, and when it gets filled, the main thread, which is waiting on
--   this MVar, will exit, closing forked server as well.
--   Server is run in `try` block, so any exception should result in `Left` exit.
prepApp :: AppConfig -> PhcEnv -> IO (AppPrep App)
prepApp cfg phcEnv = do
  r@AppPrep{..} <- prepCommonApp cfg phcEnv mkApplication
  runKatipContextT apKatipLE () "init"
    . logInfoK . fromString . toS . logConfig $ appConfig apApp
  pure r


-- | Create `App` record (holding application state in `Reader`), WAI application
--   and `Middleware`s. If `App` state creation was successful, initialises DB
--   (i.e. runs migration, inserts initial data, etc)
--   Return created `App`, WAI application and mappended list of middlewares.
mkApplication :: AppConfig -> LogEnv -> IORef DynPermMap -> PhcEnv
              -> KatipContextT IO (App, Application, Middleware)
mkApplication cfg@AppConfig{..} katipLE dpm phcEnv = do
  let fullApp = Proxy :: Proxy (ToServantApi TrackerRoutes)
      ctx     = EmptyContext

  try (mkApp cfg katipLE dpm phcEnv fullApp) >>= \case
    Left  (e :: SomeException) -> throwM e
    Right (app, middlewares)   -> do
      logInfoK "Database initialised"
      liftIO . runDBCfg (appDbPool app) $ runMigration migrateAll

      let waiApp  = mkAppM fullApp ctx app $ trackerApi
      pure (app, waiApp, foldl1 (.) middlewares)

  where
    mkAppM :: forall api a. (HasServer api a)
           => Proxy api -> Context a -> App -> ServerT api AppM -> Application
    mkAppM api sctx appCfg =
      serveWithContext api sctx . hoistServerWithContext api (Proxy @a) convertApp
      where
        convertApp :: AppM v -> Handler v
        convertApp app = Handler . ExceptT . try $ runReaderT (unRIO app) appCfg


-- | Prepare `App` state from external configuration (file and ENV). If configured,
--   forks EKG server, prepares metrics, DB pool and logging.
--   Prepares a list of possible WAI middlewares:
--     - metrics
--     - Basic HTTP Auth for the whole site with 1 credential
--     - add predefined static `Headers` to each response
--     - WAI request logger
--     - specified `Request` timeout
--     - gzip payload
--   Return created `App` and list of `Middleware's.
mkApp :: (HasEndpoint api)
      => AppConfig
      -> LogEnv
      -> IORef DynPermMap
      -> PhcEnv
      -> Proxy api
      -> KatipContextT IO (App, [Middleware])
mkApp cfg@AppConfig{..} katipLE dpm phcEnv fullApi = do
  let CommonConfig{..} = cfgCommon
  (cmnApp, mWares) <- mkCommonApp cfgCommon katipLE dpm phcEnv fullApi
  pool             <- if cCfgIsDev
    then runStdoutLoggingT . makePool $ mkPgConfig cfgDb
    else runNoLoggingT     . makePool $ mkPgConfig cfgDb

  let app = App { appCommon    = cmnApp
                , appConfig    = cfg
                , appDbPool    = DbPool pool
                }

  pure ( app, mWares)



-- | Create log line with current configuration from YAML and/or ENV
logConfig :: AppConfig -> Text
logConfig cfg@AppConfig{..} = startupLog cfgCommon <> dbStartupLog cfg
