module App.Routes where

import App.Imports

import Servant.API.Generic    (ToServant)
import Servant.Server.Generic (AsServerT, genericServerT)

import Lib.Handlers.List      (listOwnerInitiator, listOwner, listSession)
import Lib.Handlers.Track     (startSession, endSession, startEvent, endEvent)
import Lib.Handlers.Log       (setDpdH, getDpdH)
import Tracker.API            (CRURoutes (..), ListRoutes (..), LogRoutes (..))

------------------------------------------------------------------------------------------
-- Routes
------------------------------------------------------------------------------------------
listApi :: ToServant ListRoutes (AsServerT AppM)
listApi = genericServerT ListRoutes
  { listSessionR        = listSession
  , listOwnerR          = listOwner
  , listOwnerInitiatorR = listOwnerInitiator
  }

cruApi :: ToServant CRURoutes (AsServerT AppM)
cruApi = genericServerT CRURoutes
  { cruStartSessionR = startSession
  , cruEndSessionR   = endSession
  , cruStartEventR   = startEvent
  , cruEndEventR     = endEvent
  }

logApi :: ToServant LogRoutes (AsServerT AppM)
logApi = genericServerT LogRoutes
  { logSetR = setDpdH
  , logGetR = getDpdH
  }
