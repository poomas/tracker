module App.App where

import App.Import.CommonImports

import Common.PGUtils           (DbPool, HasDbPool (..))
import Common.Type.App          (CommonApp (..), HasCommonApp (..), HasCommonAppL (..),
                                 localKatip, viewKatip)
import Lib.Type                 (klsContext, klsLogEnv, klsNamespace)
import Lib.Type.Config          (AppConfig (..))

-- | Application with `App` as `Reader` payload
type AppM = RIO App

-- | Application state, both static and dynamic parts
data App = App
  { --  Static
    appCommon :: CommonApp -- ^ common data in state
  , appConfig :: AppConfig -- ^ configuration from config.yaml
  , appDbPool :: DbPool    -- ^ db pool for persistent
  }


-- | Instances

instance HasCommonAppL App  where commonAppL = lens appCommon (\x y -> x{appCommon = y})
instance HasCommonApp  AppM where getCommonApp        = asks appCommon
instance HasDbPool     AppM where getDbPool           = asks appDbPool
instance MonadMetrics  AppM where getMetrics          = capMetrics <$> asks appCommon
instance Katip         AppM where getLogEnv           = viewKatip  klsLogEnv
                                  localLogEnv         = localKatip klsLogEnv
instance KatipContext  AppM where getKatipContext     = viewKatip  klsContext
                                  getKatipNamespace   = viewKatip  klsNamespace
                                  localKatipContext   = localKatip klsContext
                                  localKatipNamespace = localKatip klsNamespace
