module App.Hooks where

import App.Imports

import Data.Aeson       (Object, eitherDecode)
import Data.String.Conv (toS)
import Katip            (logKatipItem)
import RIO              (runRIO)


import App.App          (App)
import Common.Functions (handleStdMessages, setDynamicPermitData)
import Lib.Type.Orphans ()
import Poomas.API       (LogFilterLoad (..), EventData (..), MessageData (..),
                         MessageLoad (..), MonitorLoad (..), PoomasEventLoad (..),
                         PoomasLogLoad (..), PoomasTrackLoad (..), PoomasExceptionLoad(..))
import Poomas.Server    (MessageHook, MonitorHook)


type PoomasLogItem = Item Object


messageHook :: App -> MessageHook
messageHook app ml@MessageLoad{..} = runRIO app $ do
  case msgLData of
    MessageLog lfl@LogFilterLoad{..} -> do
      b <- setDynamicPermitData "poomas" lfl
      logDebugKS "Tracker" $ "Hook: Change DPD: " <> tshow b
      pure ()
    MessageCustom t -> do
      logDebugKS "Tracker" $ "Custom message from " <> toS (showBaseUrl msgLFrom) <> ":\n"
                           <> t
    _ -> void $ handleStdMessages ml


monitorHook :: App -> MonitorHook
monitorHook app MonitorLoad{..} = runRIO app $ do
  case mlData of
    EventPoomas PoomasEventLoad{..} -> do
      logDebugKS "Tracker" $ "** Swarm change: " <> pelMessage
                          <> " >>> " <> intercalate "; " pelHubs
    EventTrack PoomasTrackLoad{..} -> do
      logDebugKS "Tracker" $ "TRACK: " <> tshow ptlUuid <> "  " <> ptlMessage
    EventLog PoomasLogLoad{..} -> do
      case eitherDecode @PoomasLogItem $ toS pllMessage of
        Right item -> logKatipItem item
        Left  e    -> logDebugKS "Tracker" $ "LOG from " <> pllOrigin <> ":\n"
                                          <> "BAD load: " <> pllMessage
                                          <> "\nError: " <> toS e
    EventException PoomasExceptionLoad{..} -> do
      say $ "Received an exception: " <> pexlMessage
      logDebugKS "Tracker" $ "Exception: " <> pexlOrigin <> "    " <> pexlMessage
    a -> logDebugKS "Tracker" $ "Other: "  <> tshow a
